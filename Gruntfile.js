'use strict';

module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch: {
      css: {
        options: {
          livereload: true
        },
        files: '**/*.scss',
        tasks: ['compass']
      }
    },
    compass: {
      options: {
        config: 'config.rb',
        bundleExec: true,
        force: true
      },
      // dev: {
      //   options: {
      //     environment: 'development'
      //   }
      // },
      dist: {
        options: {
          environment: 'production'
        }
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default', ['watch']);
};
