<?php

/**
 * @file
 * Default theme implementation for field collection items.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) field collection item label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-field-collection-item
 *   - field-collection-item-{field_name}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 * Over ridding from modules/contrib/field_collections/field-collection-item.tpl.php
 */
?>
<div class="item">
  <a href="<?php array_key_exists('field_fc_sec4_right_prod_link' , $variables) ? print $variables['field_fc_sec4_right_prod_link'][0]['value'] : print '#'; ?>">
    <?php if(array_key_exists('field_fc_sec4_right_prod_image', $content)): ?>
      <?php print render($content['field_fc_sec4_right_prod_image']); ?>
    <?php endif; ?>
    <?php if(array_key_exists('field_fc_sec4_right_prod_name', $content)): ?>
      <div> <?php print render($content['field_fc_sec4_right_prod_name']); ?></div>
    <?php endif; ?>
    <?php if(array_key_exists('field_fc_sec4_right_prod_price', $content)): ?>
      <div class="money-wrapper"> $<?php print render($content['field_fc_sec4_right_prod_price']); ?></div>
    <?php endif; ?>
  </a>
</div>

