<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
global $base_url;
?>
<link rel="stylesheet" href="http://demo.dev7studios.com/nivo-slider/wp-content/plugins/nivo-slider/scripts/nivo-slider/nivo-slider.css?ver=4.2.3" type="text/css" />
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script> -->
<?php print $messages; ?> <!-- For Debugging Purpose -->
<div class="mainContentRegion">
  <?php
  $vars = array(
  'content' => reset($page['content']['system_main']['nodes'])
  );
  print theme('section1_template' , array('pageVariable' => $vars));
  ?>
  <div class="contentRegionInner">
    <?php
    print render($page['content']);
    ?>
<!--     <section class="col-s-4 col-l-12 sec3">
      <div class="sec3-inner">
      <div id="slider2" class="nivoSlider">
          <img src="<?php print $base_url.'/'.drupal_get_path('theme','fila_server'); ?>/files/sec-small-car/secssimg1.png" alt="" title="#slide1-caption"/>
          <img src="<?php print $base_url.'/'.drupal_get_path('theme','fila_server'); ?>/files/sec-small-car/secssimg2.png" alt="" title="#slide2-caption"/>
          <img src="<?php print $base_url.'/'.drupal_get_path('theme','fila_server'); ?>/files/sec-small-car/secssimg1.png" alt="" title="#slide3-caption"/>
      </div>
      <div id="slide1-caption" class="nivo-html-caption">
        <div class="synopsis">
          - in the spotlight
        </div>
        <div class="synopsis-main-text">
          <h2> Slide no. 1 </h2>
          <p>
          Distinctively pursue revolutionary sources through interoperable convergence. Efficiently conceptualize strategic users after scalable ROI.</p>
          <p>Objectively develop bleeding-edge convergence via top-line convergence. Globally morph 24/7 leadership skills.
        </p>
        </div>
      </div>
      <div id="slide2-caption" class="nivo-html-caption">
        <div class="synopsis">
          - in the spotlight
        </div>
        <div class="synopsis-main-text">
          <h2> Slide no. 2 </h2>
          <p>
          Distinctively pursue revolutionary sources through interoperable convergence. Efficiently conceptualize strategic users after scalable ROI.</p>
          <p>Objectively develop bleeding-edge convergence via top-line convergence. Globally morph 24/7 leadership skills.
        </p>
        </div>
      </div>
      <div id="slide3-caption" class="nivo-html-caption">
        <div class="synopsis">
          - in the spotlight
        </div>
        <div class="synopsis-main-text">
          <h2> Slide no. 3 </h2>
          <p>
          Distinctively pursue revolutionary sources through interoperable convergence. Efficiently conceptualize strategic users after scalable ROI.</p>
          <p>Objectively develop bleeding-edge convergence via top-line convergence. Globally morph 24/7 leadership skills.
        </p>
        </div>
      </div>
      </div>
    </section> -->
<!--     <section class="col-s-4 col-l-12 sec4">
      <div class="men-section section-half">
        <div class="image-text-container">
          <img src="<?php print $base_url.'/'.drupal_get_path('theme','fila_server'); ?>/files/sec3/men.png" alt="" />
          <div class="sec3-overlay">
            <div>Men Outwear</div>
            <div class="explore">Explore</div>
          </div>
        </div>
        <div class="section-items">
          <div class="close-button"><div class="close-btn"></div><div class="close-btn-text">Close</div></div>
          <div class="item"><img src="<?php print $base_url.'/'.drupal_get_path('theme','fila_server'); ?>/files/sec3/t-shirt.png" alt="" /><div>collezione Peplum Top</div><div class="money-wrapper">$39.5</div></div>
          <div class="item"><img src="<?php print $base_url.'/'.drupal_get_path('theme','fila_server'); ?>/files/sec3/shoe.png" alt="" /><div>F100D tennis Shoe</div><div class="money-wrapper">$65.00</div></div>
          <div class="item"><img src="<?php print $base_url.'/'.drupal_get_path('theme','fila_server'); ?>/files/sec3/sleeveless.png" alt="" /><div>Tennis Top</div><div class="money-wrapper">$25.00</div></div>
          <div class="item"><img src="<?php print $base_url.'/'.drupal_get_path('theme','fila_server'); ?>/files/sec3/cardigan.png" alt="" /><div>Heritage Cardigan</div><div class="money-wrapper">$165.00</div></div>
        </div>
      </div>
      <div class="women-section section-half">
        <div class="image-text-container">
          <img src="<?php print $base_url.'/'.drupal_get_path('theme','fila_server'); ?>/files/sec3/women.png" alt="" />
          <div class="sec3-overlay">
            <div>Women Heritage Collection</div>
            <div class="explore">Explore</div>
          </div>
        </div>
      </div>
    </section> -->
    <section class="col-s-4 col-l-12 sec5">
      <div class="main-red-text">Free Shipping on order of $80 or more</div>
      <span class="tnc-app">* Exclusion Applied <a href="#">Learn More</a></span>
    </section>
    <section class="col-s-4 col-l-12 sec6">
      <div class="sec6-inner">
        <div class="sec6-header">
          <div class="synopsis"> - social graces </div>
          <h2 class="main-bold-text">Stop Not Following Us</h2>
          <a href="#">Instagram</a>
          <a href="#">Vimeo</a>
          <a href="#">Facebook</a>
          <a href="#">Pintrest</a>
          <a href="#">Twitter</a>
        </div>
        <div class="grid-container">
          <div class="item-1 item">
            <div class="image-detail-container">
              <img src="<?php print $base_url.'/'.drupal_get_path('theme','fila_server');?>/files/sec5/small-inst.png" />
              <div class="detail-container">
                <div class="synopsis">- upcoming</div>
                <div class="synopsis-main-content">@filausa</div>
              </div>
            </div>
          </div>
          <div class="item-2 item">
            <div class="image-detail-container">
              <img src="<?php print $base_url.'/'.drupal_get_path('theme','fila_server');?>/files/sec5/med-inst.png" />
              <div class="detail-container">
                <div class="synopsis">- upcoming</div>
                <div class="synopsis-main-content">@filakorea</div>
              </div>
            </div>
          </div>
          <div class="item-3 item">
            <div class="image-detail-container">
              <img src="<?php print $base_url.'/'.drupal_get_path('theme','fila_server');?>/files/sec5/large-inst.png" />
              <div class="synopsis">- upcoming</div>
              <div class="synopsis-main-content">Read "The Filaphile"</div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>


