<?php
 $content = $pageVariable['content'];
 $field_fc_sec2_slides = $pageVariable['field_fc_sec2_slides']
?>

<?php if($st === '1') : ?>
  <!-- Section 2 Start -->
  <section class="col-s-4 col-l-12 sec2">
    <div class="info-wrapper">
      <div class="info-inner">
        <div class="rounded-info">Drag to Explore</div>
      </div>
    </div>
    <div class="sec2-content">
      <div class="sec2-inner">
        <?php unset($content['field_fc_sec2_slides']['#prefix']);  // Unsetting the prefix Part?>
        <?php unset($content['field_fc_sec2_slides']['#suffix']);  // Unsetting the suffix Part?>
        <?php $length = count($field_fc_sec2_slides);
            for ($x = 0; $x <= $length; $x++) {
               unset($content['field_fc_sec2_slides'][$x]['links']);
             }
         ?>
        <?php print render($content['field_fc_sec2_slides']); ?>
      </div>
    </div>
  </section>
<!-- Section 2 Ends -->
<?php endif; ?>
