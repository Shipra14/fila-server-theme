<?php
 $content = $pageVariable['content'];
?>
<?php if($st === '1') : ?>
<!-- Section 4 Start -->
  <section class="col-s-4 col-l-12 sec4">
    <?php unset($content['field_fc_sec4_left_prod']['#prefix']);  // Unsetting the prefix Part?>
    <?php unset($content['field_fc_sec4_left_prod']['#suffix']); //Unsetting the suffix Part?>
    <?php unset($content['field_fc_sec4_right_prod']['#prefix']);  // Unsetting the prefix Part?>
    <?php unset($content['field_fc_sec4_right_prod']['#suffix']); //Unsetting the suffix Part?>
    <div class="men-section section-half">
      <div class="image-text-container">
        <?php print render($content['field_sec4_left_image']); ?>
      </div>
      <div class="sec3-overlay">
        <?php print render($content['field_sec4_left_eyebrow']); ?>
        <?php print render($content['field_sec4_left_text']); ?>
      </div>
      <div class="section-items">
        <div class="close-button"><div class="close-btn"></div><div class="close-btn-text">Close</div></div>
        <?php print render($content['field_fc_sec4_left_prod']); ?>
        <?php if(array_key_exists('field_sec4_left_link',$content)): ?>
        <div class="shop-all-link">
          <a href="<?php print $content['field_sec4_left_link'][0]['#markup'];?>"> Shop All </a>
        </div>
        <?php endif; ?>
      </div>
    </div>


    <div class="women-section section-half">
      <div class="image-text-container">
        <?php print render($content['field_sec4_right_image']); ?>
      </div>
      <div class="sec3-overlay">
        <?php print render($content['field_sec4_right_eyebrow']); ?>
        <?php print render($content['field_sec4_right_text']); ?>
      </div>
      <div class="section-items">
        <div class="close-button"><div class="close-btn"></div><div class="close-btn-text">Close</div></div>
        <?php print render($content['field_fc_sec4_right_prod']); ?>
        <?php if(array_key_exists('field_sec4_right_link',$content)): ?>
        <div class="shop-all-link">
          <a href="<?php print $content['field_sec4_right_link'][0]['#markup'];?>"> Shop All </a>
        </div>
        <?php endif; ?>
      </div>
    </div>
  </section>
  <!-- Section 4 Ends -->
<?php endif; ?>
