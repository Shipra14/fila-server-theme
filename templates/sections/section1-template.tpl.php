<?php
  $content = $pageVariable['content'];
?>
<!-- Section 1 Start -->
<section class="col-s-4 col-l-12 sec1 <?php //print ($theme_class);?>">
  <?php unset($content['field_fc_sec1_slides']['#prefix']);  // Unsetting the prefix Part?>
  <?php unset($content['field_fc_sec1_slides']['#suffix']); //Unsetting the suffix Part?>
  <?php print render($content['field_fc_sec1_slides']); ?>
</section>
<!-- Section 1 Ends -->
